# Question

## [Configure cache on GitLab runner - install dependencies by composer only once](https://stackoverflow.com/q/58885235/157675)

I have problem with gitlab cache config.

I use GitLab Community Edition 11.7.7, in docs we have: 
https://docs.gitlab.com/ee/ci/caching/#caching-php-dependencies

So, my config:

```
cache:
  key: ${CI_COMMIT_REF_SLUG}
  paths:
    - vendor/

stages:
  - code-style
  - analysis

before_script:
  - composer global require hirak/prestissimo
  - composer install --ignore-platform-reqs --no-scripts

php-cs-fixer:
  stage: code-style
  script:
    - vendor/bin/php-cs-fixer fix --dry-run --diff --verbose --config=.php_cs.dist src tests

php-cs-fixer2:
  stage: analysis
  script:
    - vendor/bin/php-cs-fixer fix --dry-run --diff --verbose --config=.php_cs.dist src tests
```
Unfortunately, composer re-installs all dependencies in every job.

I would like to install all dependencies through the composer only once, how can I achieve it?
